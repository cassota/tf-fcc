#!/usr/bin/env python3
#
# author: july 2020
# cassio batista - https://cassota.gitlab.io
# last edited: july 2020

import sys
import numpy as np
import tensorflow as tf
from tensorflow import keras

# avoid numpy truncating output to stdout with dots
np.set_printoptions(threshold=sys.maxsize)

# load dataset
(train, y_train), (test, y_test) = keras.datasets.fashion_mnist.load_data()

# scale: normalize pixels to range [0. 1]
# NOTE: can't =/ bc 'ValueError: output array is read-only'
train = train / 255.0
test = test / 255.0

# https://keras.io/api/datasets/fashion_mnist/
LABELS = [ 'T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal',
            'Shirt', 'Sneaker', 'Bag', 'Ankle boot', ]

# define the architecture
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='softmax'),
])

# TODO does something
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# train
model.fit(train, y_train, epochs=5)

# evaluate accuracy over all test dataset
loss, acc = model.evaluate(test, y_test, verbose=1)
print(loss, acc)

# evaluate over single sample
pred = model.predict(test)
print(pred[0], LABELS[np.argmax(pred[0])])
