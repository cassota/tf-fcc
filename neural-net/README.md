# Feed-Forward Neural Net

```mermaid
graph LR;
    A[load<br>data] --> B[normalise];
    B               --> C[define<br>architecture];
    C               --> D[compile via<br>Sequential class];
    D               --> E[train via<br>fit method];
    E               --> F[test overall via<br>evaluate method];
    E               --> G[test single instance<br>via predict method];
```
