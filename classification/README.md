# Classification via DNN

```mermaid
graph LR;
    A[load<br>CSV] --> B[separate<br>label<br>column];
    B              --> C[build<br>feature<br>columns];
    C              --> D[make<br>input<br>function];
    D              --> E[train<br>DNN<br>classifier];
    E              --> F[evaluate<br>DNN];
```
