#!/usr/bin/env python3
#
# author: july 2020
# cassio batista - https://cassota.gitlab.io

import sys
import os

__author__ = 'Cassio Batista'
__email__ = 'cassio.batista.13@gmail.com'

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.abspath(os.path.join(SRC_DIR, os.pardir))

DATA_DIR = os.path.join(ROOT_DIR, 'data', 'iris')
