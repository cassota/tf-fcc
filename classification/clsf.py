#!/usr/bin/env python3
#
# author: july 2020
# cassio batista - https://cassota.gitlab.io
# last edited: july 2020

import sys
import os
import numpy as np
import pandas as pd
import tensorflow as tf

import config

# first line of CSV files doesn't actually state the name of each column so
# we'll have o do it manually
COLUMNS = ['sepal_l', 'sepal_w', 'petal_l', 'petal_w', 'species']
LABELS = ['Setosa', 'Versicolor', 'Virginica']

# load datsets
train = pd.read_csv(os.path.join(config.DATA_DIR, 'iris_training.csv'),
                    header=0, names=COLUMNS)
test = pd.read_csv(os.path.join(config.DATA_DIR, 'iris_test.csv'),
                    header=0, names=COLUMNS)

# strip off the labels column to a separate data structure
y_train = train.pop('species')
y_test = test.pop('species')

# everything is good to go in this iris models since all features are numerical
# already
feat_cols = []
for key in train.keys():
    feat_cols.append(tf.feature_column.numeric_column(key=key))

def input_fn(data, labels, training=True, bs=256):
    ds = tf.data.Dataset.from_tensor_slices((dict(data), labels))
    if training:
        ds = ds.shuffle(1000).repeat()
    return ds.batch(bs)

# build a classifier object
c = tf.estimator.DNNClassifier(feature_columns=feat_cols,
                               hidden_units=[30, 10], n_classes=3)

# train the model
c.train(input_fn=lambda: input_fn(train, y_train), steps=5000)

# evaluate the model: get overall accuracy from the entire dataset
# NOTE: use 'predict' method for one by one result
result = c.evaluate(input_fn=lambda: input_fn(test, y_test, training=False))
for key, val in result.items():
    if key == 'accuracy':
        print(key, val)
