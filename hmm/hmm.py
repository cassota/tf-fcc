#!/usr/bin/env python3
#
# author: july 2020
# cassio batista - https://cassota.gitlab.io

import tensorflow as tf
import tensorflow_probability as tfp  # NOTE: v0.10 is the up to date via pip

di = tfp.distributions.Categorical(probs=[0.8, 0.2])
dt = tfp.distributions.Categorical(probs=[[0.7, 0.3], [0.2, 0.8]])
do = tfp.distributions.Normal(loc=[0.0, 15.0], scale=[5.0, 10.0])

model = tfp.distributions.HiddenMarkovModel(initial_distribution=di,
                                            transition_distribution=dt,
                                            observation_distribution=do,
                                            num_steps=7)

mean = model.mean()

with tf.compat.v1.Session() as sess:
    print(mean.numpy())
