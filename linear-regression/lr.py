#!/usr/bin/env python3
#
# author: july 2020
# cassio batista - https://cassota.gitlab.io
# last edited: july 2020

import sys
import os
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

import config

# load datasets
train = pd.read_csv(os.path.join(config.DATA_DIR, 'train.csv'))
test = pd.read_csv(os.path.join(config.DATA_DIR, 'eval.csv'))

# set labels columns apart: this is actually what we are trying to predict so
# it must be separated from the train and test data dataframes
y_train = train.pop('survived')
y_test = test.pop('survived')

# here we're gonna create feature columns for tensorflow
CAT_FEATS = [ 'sex', 'n_siblings_spouses', 'parch', 'class', 'deck', 'embark_town', 'alone' ]
NUM_FEATS = [ 'age', 'fare' ]
feat_cols = []

# for categorical features
for feat in CAT_FEATS:
    voc = train[feat].unique()
    feat_cols.append(tf.feature_column
                       .categorical_column_with_vocabulary_list(feat, voc))

# for numerical features
for feat in NUM_FEATS:
    feat_cols.append(tf.feature_column.numeric_column(feat, dtype=tf.float32))

# NOTE: this nested functions is gonna be replaced by a lambda directive later
def make_input_fn(data, labels, num_epochs=10, shuffle=True, bs=32):
    def input_function():
        ds = tf.data.Dataset.from_tensor_slices((dict(data), labels))
        if shuffle:
            ds = ds.shuffle(1000)
        ds = ds.batch(bs).repeat(num_epochs)
        return ds
    return input_function

# make input functin considering batch size, number of epochs and data in
# format tf.data.Dataset, all good the way tensroflow likes
train_input_fn = make_input_fn(train, y_train)
test_input_fn = make_input_fn(test, y_test, num_epochs=1, shuffle=False)

# create the linera estimator object
le = tf.estimator.LinearClassifier(feature_columns=feat_cols)

# train the model, which will be available though 'le' object
le.train(train_input_fn)

# evaluate overall accuracy (average)
result = le.evaluate(test_input_fn)
for key, val in result.items():
    print('%20s' % key, val)

# alternative evaluation one by one
# evaluate accuracy for each instance in test dataset
result = le.evaluate(test_input_fn)
result = list(le.predict(test_input_fn))
for r in result:
    for key, val in r.items():
        if key == 'probabilities':
            print(key, val)
