# Linear Regression

```mermaid
graph LR;
    A[load<br>CSV] --> B[separate<br>label<br>column];
    B              --> C[create<br>feature<br>columns];
    C              --> D[make<br>input<br>function];
    D              --> E[train<br>linear<br>classifier];
    E              --> F1[evaluate<br>linear classifier<br>overall];
    E              --> F2[evaluate<br>linear classifier<br>one by one];
```
